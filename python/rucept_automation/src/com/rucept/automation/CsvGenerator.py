import csv
import os
import time

import Utils
import config_data.Constants as Constants

constants = Constants.Constants()

utils = Utils.Utils()


class CSVGenerator:
    def __init__(self):
        self.data = []
    
    @staticmethod
    def initCSV(csv_out_path, csv_name):
        with open(csv_out_path + "/" + csv_name, "wb") as f:
            writer = csv.writer(f)
            writer.writerows(constants.getAmazonCSVColumns())
    
    @classmethod
    def generateCSVs(cls, csv_out_path, csv_name, phone_name, dsn_image_name):
        filename = csv_out_path + "/" + csv_name
        if not os.path.exists(filename):
            cls.initCSV(csv_out_path, csv_name)
        
        with open(filename, "a") as file:
            writer = csv.writer(file)
            a = [[]]
            row = a[0]
            row.append(cls.get_sku(phone_name))
            cls.fill_empty_values_in_row(row, 2)
            row.append(cls.fill_item_name_in_row(dsn_image_name, phone_name))
            print dsn_image_name
            print phone_name
            cls.fill_empty_values_in_row(row, 0)
            writer.writerows(a)
    
    @classmethod
    def fill_empty_values_in_row(cls, a, count):
        if count < 1:
            col_len = constants.getAmazonCSVColumns()[0].__len__()
            a_len = a.__len__()
            for num in range(a_len, col_len):
                a.append("")
        else:
            for num in range(0, count):
                a.append("")
    
    @classmethod
    def get_sku(cls, phone_name):
        return str(phone_name).replace(".png", "") + str(time.time().real)
    
    @classmethod
    def fill_item_name_in_row(cls, dsn_image, phn_image):
        dsn_image = dsn_image.replace(".png", "")
        dsn_image = dsn_image.replace(".jpg", "")
        phn_image = phn_image.replace(".png", "")
        phn_image = phn_image.replace(".jpg", "")
        return '%s Printed Back Cover Case For %s' % (dsn_image, phn_image)