from imutils import paths
import numpy as np
import argparse
import cv2
import os

from src.com.rucept.automation import Utils


utils = Utils.Utils()

# load the watermark image, making sure we retain the 4th channel
# which contains the alpha transparency
watermark = cv2.imread("sample_code/images/HWIAM7UCASE.png", cv2.IMREAD_UNCHANGED)
(wH, wW) = watermark.shape[:2]

# split the watermark into its respective Blue, Green, Red, and
# Alpha channels; then take the bitwise AND between all channels
# and the Alpha channels to construct the actaul watermark
# NOTE: I'm not sure why we have to do this, but if we don't,
# pixels are marked as opaque when they shouldn't be
(B, G, R, A) = cv2.split(watermark)
B = cv2.bitwise_and(B, B, mask=A)
G = cv2.bitwise_and(G, G, mask=A)
R = cv2.bitwise_and(R, R, mask=A)
watermark = cv2.merge([B, G, R, A])

image = cv2.imread("sample_code/images/AbstractPrint.jpg", cv2.IMREAD_COLOR)
output = image.copy()
cv2.addWeighted(watermark, 10, output, 1.0, 0, output)

utils.show_image(output,"out",10)
