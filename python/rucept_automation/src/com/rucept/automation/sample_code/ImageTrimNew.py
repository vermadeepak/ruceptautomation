import cv2
import numpy as np
import time
import src.com.rucept.automation.Utils

import Image

import csv


def prepare_name(name, op_choice):
    str_new = None
    if op_choice == 1:
        str_new = "Center_fit"
    elif op_choice == 2:
        str_new = "top_left_fit"
    elif op_choice == 3:
        str_new = "right_top_fit"
    elif op_choice == 4:
        str_new = "bottom_left_fit"
    elif op_choice == 5:
        str_new = "bottom_right_fit"
    
    return name.replace(str("_" + str(op_choice) + "_"), str("_" + str_new + "_"))


utils = src.com.rucept.automation.Utils.Utils()

input_base_folder = "/home/local/JASPERINDIA/verma.deepak/rucept_data/input/"
output_base_folder = "/home/local/JASPERINDIA/verma.deepak/rucept_data/output/"
temp_base_folder = "/home/local/JASPERINDIA/verma.deepak/rucept_data/temp/"

# Read the image, convert it into grayscale, and make in binary image for threshold value of 1.
img = cv2.imread(input_base_folder + 'HalfButterfly.jpg', cv2.IMREAD_UNCHANGED)
phn_image = cv2.imread('sample_code/images/HWIAM7UCASE.png', cv2.IMREAD_UNCHANGED)

pix = np.asarray(phn_image)

pix = pix[:, :, 0:3]  # Drop the alpha channel
idx = np.where(pix - 255)[0:2]  # Drop the color when finding edges
box = map(min, idx)[::-1] + map(max, idx)[::-1]

trimmed_phn_image = phn_image[box[1]:box[3] - 1, box[0]:box[2] + 1]
trimmed_phn_image_path = temp_base_folder + str(time.time()) + ".png"
utils.save_image(trimmed_phn_image, trimmed_phn_image_path)

height, width, depth = trimmed_phn_image.shape[:3]
print (height, width, depth)

print ("To center fit, press 1")
print ("To center top left fit, press 2")
print ("To center right top fit, press 3")
print ("To center bottom left fit, press 4")
print ("To center bottom right fit, press 5")

cropped = None

ih, iw = img.shape[:2]

while True:
    
    choice = int(input("choice"))
    
    if choice == 1:
        icx = iw / 2
        ich = ih / 2
        
        cropped = img[ich - (height / 2):ich + (height / 2) - 1, icx - width / 2:icx + width / 2 + 1]
    
    elif choice == 2:
        cropped = img[0:height, 0:width]
    
    elif choice == 3:
        cropped = img[0:height, iw - width:iw]
    
    elif choice == 4:
        cropped = img[ih - height:ih, 0:width]
    
    elif choice == 5:
        cropped = img[ih - height:ih, iw - width:iw]
    
    cropped_img_path = prepare_name(str(temp_base_folder + "cropped_" + str(choice) + "_" + str(time.time()) + ".png"),
                                    choice)
    utils.save_image(cropped, cropped_img_path)
    
    ##################################################################################################
    ############ Merging starts here #################################################################
    
    im = Image.open(cropped_img_path)
    im.save(cropped_img_path)
    img_dest = im.copy().convert('RGBA')
    
    res = None
    
    merged_img_path = prepare_name(str(output_base_folder + "merged_" + str(choice) + "_" + str(time.time()) + ".png"),
                                   choice)
    # utils.save_image(res, merged_img_path)

    background = Image.open(cropped_img_path)
    foreground = Image.open(trimmed_phn_image_path)

    background.paste(foreground, (0, 0), foreground)
    # background.show()
    background.save(merged_img_path)