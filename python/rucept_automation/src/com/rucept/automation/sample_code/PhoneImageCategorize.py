import cv2
import numpy as np
import time
import src.com.rucept.automation.Utils
import Image
import os

utils = src.com.rucept.automation.Utils.Utils()

phone_images_folder = "/home/local/JASPERINDIA/verma.deepak/rucept_data/phones/"
temp_base_folder = "/home/local/JASPERINDIA/verma.deepak/rucept_data/temp/"

path = None

for fn in os.listdir(phone_images_folder):
    path = phone_images_folder + fn
    img = cv2.imread(path, cv2.IMREAD_UNCHANGED)

    pix = np.asarray(img)

    pix = pix[:, :, 0:3]  # Drop the alpha channel
    idx = np.where(pix - 255)[0:2]  # Drop the color when finding edges
    box = map(min, idx)[::-1] + map(max, idx)[::-1]

    trimmed_phn_image = img[box[1]:box[3] - 1, box[0]:box[2] + 1]
    
    img = trimmed_phn_image

    trimmed_phn_image_path = temp_base_folder + "temp.png"
    utils.save_image(trimmed_phn_image, trimmed_phn_image_path)

    
    im = Image.open(trimmed_phn_image_path)
    r, g, b, a = im.split()
    print a
    img_dest = im.copy().convert('RGBA')
    
    break
    
    
    
    


