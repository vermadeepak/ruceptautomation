import cv2
from matplotlib import pyplot as plt

from src.com.rucept.automation import Utils

utils = Utils.Utils()

phn_image = cv2.imread("images/HWIAM7UCASE.png", cv2.IMREAD_UNCHANGED)
img = cv2.imread("images/AbstractPrint.jpg", -1)
print ("Image", img.shape)

dst = phn_image

# resize(src, dst, dst.size(), 0, 0, interpolation);
height, width = phn_image.shape[:2]

height, width, depth = phn_image.shape[:3]
resized = cv2.resize(img, (width, height), interpolation=cv2.INTER_CUBIC)

print ("resized", resized.shape)
print ("phn_img", phn_image.shape)

for i in range(len(phn_image)):
    if phn_image[i].alpha():
        resized[i] = [0, 0, 0]

print ("resized", resized.shape)


plt.subplot(121), plt.imshow(resized, cmap='gray')
plt.title('resized'), plt.xticks([]), plt.yticks([])
plt.subplot(122), plt.imshow(img, cmap='gray')
plt.title('original'), plt.xticks([]), plt.yticks([])

plt.show()

# utils.show_image(resized, "Image", 20)

# s_img = cv2.imread("images/HWIAM7UCASE.png")
# l_img = cv2.imread("images/AbstractPrint.jpg")
# x_offset = y_offset = 50
# l_img[y_offset:y_offset + s_img.shape[0], x_offset:x_offset + s_img.shape[1]] = s_img
#
# # cv2.imshow("original", s_img)
# # cv2.waitKey(0)
#
#
# height, width, depth = img.shape
# circle_img = np.zeros((height, width), np.uint8)
# cv2.circle(circle_img, (width / 2, height / 2), 280, 1, thickness=-1)
#
# masked_data = cv2.bitwise_and(phn_image, phn_image, mask=img)
#
# utils.show_image(masked_data, "Masked", 10)
