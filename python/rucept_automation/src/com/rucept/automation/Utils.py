import cv2
import numpy as np


class Utils:
    def __init__(self):
        self.data = []
    
    @classmethod
    def show_image(cls, src, title, wait_seconds):
        if title is None:
            title = "Image"
        cv2.imshow(title, src)
        cv2.waitKey(wait_seconds * 1000)
        return
    
    @classmethod
    def save_image(cls, img, path):
        cv2.imwrite(path, img)
    
    # @classmethod
    # def save_image(cls, img, base_folder, name):
    #     cv2.imwrite(str(base_folder + "/" + name), img)


Utils().__init__()
