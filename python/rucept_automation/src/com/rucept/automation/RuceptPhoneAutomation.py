import os

import CsvGenerator
import FileImposer
import Utils

imposer = FileImposer.FileImposer()
csv_generator = CsvGenerator.CSVGenerator()
utils = Utils.Utils()

req_id = None

#### Init with the basic directories
base_folder = "/var/tmp/rucept_automation_phone_req/"
req_folder = base_folder + "req/"
phones_folder = base_folder + "phones/"

############# If not created, create the specific directories
temp_folder = base_folder + "temp/"
if not os.path.exists(temp_folder):
    os.makedirs(temp_folder)

csv_out_folder = base_folder + "csv_output/"
if not os.path.exists(csv_out_folder):
    os.makedirs(csv_out_folder)

# if req_id has been passed by command, extract it

# If request id is still NULL, process the req folders one by one
if req_id is not None:
    d = 9
    ######################id specific work only

else:
    for req_id in os.listdir(req_folder):
        path = req_folder + req_id
        
        # Initiate CSV output
        csv_generator.initCSV(csv_out_folder,
                              req_id + ".csv")
        
        if not path.endswith("_done"):
            req_input_folder = req_folder + req_id + "/input/"
            req_output_folder = req_folder + req_id + "/output/"
            
            for dsn_img in os.listdir(req_input_folder):
                if dsn_img.endswith(".png") or dsn_img.endswith(".jpg"):
                    dsn_img_path = req_input_folder + dsn_img
                    
                    if not os.path.exists(req_output_folder + "/" + dsn_img):
                        os.makedirs(req_output_folder + "/" + dsn_img)
                        
                    for phn_img in os.listdir(phones_folder):
                        phn_img_path = phones_folder + phn_img
                        imposer.impose_image(base_folder,
                                             dsn_img_path,
                                             req_output_folder + "/" + dsn_img + "/",
                                             phn_img_path,
                                             phn_img)
                        csv_generator.generateCSVs(csv_out_folder,
                                                   req_id + ".csv",
                                                   phn_img,
                                                   dsn_img)
            
            # once request is processed, mark this req folder as done
            # os.rename(req_folder + req_id, req_folder + req_id + "_done")
